ARG CI_REGISTRY
FROM debian:stretch-backports

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install binutils make nasm
