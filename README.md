# Introduction

This project creates a docker image with nasm installed.

The image can be used to program assembly or as a base image for other images.

This repository is mirrored to https://gitlab.com/sw4j-net/nasm
